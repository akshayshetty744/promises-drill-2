
// problem1
function test() {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => response.json())
        .then((data) => console.log(data))
}
test()

 // problem2   
async function fetchdata() {
    let data = await fetch('https://jsonplaceholder.typicode.com/users')  
    let result =await data.json()
    console.log(result)
}
    fetchdata()