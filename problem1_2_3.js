//problem1
function getFirstName() {
    return new Promise((resolve, reject) => {
        let firstName = { name: "Arya" };
       setTimeout(() => {
           resolve(firstName.name); 
       }, 1000);
    })
} 
getFirstName().then((result) => { console.log(result) })

//problem2
function getLastName() {
    return new Promise((resolve, reject) => {
        let lastName = { name: "Stark" };
        setTimeout(() => {
            resolve(lastName.name)
      }, 2000);
    })
}
getLastName().then((result1) => { console.log(result1) })

//problem3
// async function getFullName() {
//     let firstName =await getFirstName();
//     let lastName =await getLastName();
//     let fullName = firstName + " " + lastName;
//     console.log(fullName)
// }
// getFullName()

// without  using async await
function getFullName(firstName, lastName) {
    Promise.all([firstName, lastName]).then((result) => {
        console.log(result[0]+" "+result[1])
    })
}
getFullName(getFirstName(), getLastName());
